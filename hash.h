/* Liam Healy 2018-10-01 15:03:12EDT hash.h */
/* Time-stamp: <2021-09-23 19:09:49EDT hash.h> */

#ifndef HASHH
#define HASHH
#include <gsl/gsl_block_double.h>
#include <stdbool.h>
#include "uthash.h"
#define KEYLEN 40		/* Maximum number of characters of a key */
#define VALLEN 120		/* Maximum number of characters of a value */
#define NO_CATEGORY 0		/**< Designates the parameter as not belonging to any category @hideinitializer */

/**
 * @brief All possible parameter types.
 */
enum rp_type_list
  {
   rp_type_int,			/**< Integer */
   rp_type_double,		/**< Double float */
   rp_type_double_ptr,		/**< Double float array (pointer) */
   rp_type_double_vec,          /**< GSL block, double vector with dimension */
   rp_type_float_ptr,		/**< Single float array (pointer)  */
   rp_type_bool,		/**< Boolean (logical) */
   rp_type_char_ptr,		/**< String */
  };

/**
 * @brief The value of any type of datum; the appropriate member is set.
 */
union datum_value {
  int rpt_int;	                       /**< Integer  */
  double rpt_double;                   /**< Double float  */
  double *rpt_double_ptr;	       /**< Double float array (pointer) */
  gsl_block *rpt_double_gsl;	       /**< GSL block, double vector with dimension */
  float *rpt_float_ptr;  	       /**< Single float array (pointer)  */
  bool rpt_bool;		/**< Boolean (true-false)  */
  char *rpt_string;		/**< String */
  struct rp_keyvalp *rpt_members;	/**< The hash table for the group's parameters */
};

/**
 * @brief A parameter and its properties.
 */
struct rp_parameter {
  char *name;		    /**< Name of parameter */
  enum rp_type_list type;   /**< Type of parameter data */
  size_t size;              /**< Number of elements, =1 for scalars */
  int category; 	    /**< Category of parameter (optional); set to @c NO_CATEGORY if there is no category */
};

/**
 * @brief A table of parameter names (keys) and values
 */
struct rp_keyvalp {
  char key[KEYLEN];		/**< The name of the parameter */
  union datum_value *value;	/**< Value of the parameter, NULL if key represents a group */
  UT_hash_handle hh;         /**< To make this structure hashable */
};

#define WARN_ON true
#define WARN_OFF false

int rp_parameter_int(struct rp_keyvalp *table, char *name, bool warn, int default_val);
double rp_parameter_double(struct rp_keyvalp *table, char *name, bool warn);
double *rp_parameter_double_ptr(struct rp_keyvalp *table, char *name, bool warn);
gsl_block *rp_parameter_double_gsl(struct rp_keyvalp *table, char *name, bool warn);
float *rp_parameter_float_ptr(struct rp_keyvalp *table, char *name, bool warn);
char *rp_parameter_string(struct rp_keyvalp *table, char *name, bool warn);
bool rp_parameter_bool(struct rp_keyvalp *table, char *name, bool warn, bool default_val);
struct rp_keyvalp *rp_group_members(struct rp_keyvalp *group);
struct rp_keyvalp *rp_make_parameter_properties_table
(struct rp_parameter *parameters, size_t property, size_t numitems);
struct rp_keyvalp *rp_make_sequential_table(const char **names, int numitems, int start);
int rp_find_category
(struct rp_keyvalp *parameters_values, struct rp_keyvalp *parameter_properties, bool complete);
// The following are for internal use (in hash.*, yaml.*)
struct rp_keyvalp *make_keyvalp
(struct rp_keyvalp **table, const char *key, struct rp_keyvalp *group);
#endif
