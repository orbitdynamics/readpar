/* Liam Healy 2018-07-12 14:23:52EDT yaml.h */
/* Time-stamp: <2021-09-23 19:11:20EDT yaml.h> */
#ifndef YAMLH
#define YAMLH
#include <sys/stat.h>
#include "hash.h"

struct rp_keyvalp *rp_read_parameter_yaml
(char *filename, struct rp_keyvalp *type_table, struct rp_keyvalp *length_table);
#endif
