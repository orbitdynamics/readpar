/* Hash table utilities */
/* Liam Healy 2018-10-01 15:01:00EDT hash.c */
/* Time-stamp: <2023-06-15 11:54:59EDT hash.c> */

#include <gc/gc.h>
#include <stdlib.h>
#include <math.h>
#include "zlog.h"
#include "hash.h"
#include "stdio.h"
#include "logenhance.h"

/************************************************************************
 * Read values from hash tables
 ************************************************************************/

/**
 * @brief Local function to get the value of a parameter.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @return The value of the parameter
 */
static union datum_value *find_dv(struct rp_keyvalp *table, char *name) {
    struct rp_keyvalp *s;
    HASH_FIND_STR(table, name, s);  /* s: output pointer */
    if (s == NULL) {
      return(NULL);
    }
    return (s->value);
}

#pragma GCC visibility push(default)

/**
 * @brief Get the value of an integer parameter.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @param[in] default_val The value to return if the parameter cannot be found in the table
 * @return The value of the parameter; default_val if not found
 */
int rp_parameter_int(struct rp_keyvalp *table, char *name, bool warn, int default_val) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find parameter "
			  PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			 " returning default value "
			 PARAM_VALUE_BEGIN "%d" PARAM_VALUE_END,
			 name,
			 default_val);
    return(default_val);
  }
  return(dv->rpt_int);
}

/**
 * @brief Get the value of a double parameter from the table.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @return The value of the parameter; NaN if not found
 */
double rp_parameter_double(struct rp_keyvalp *table, char *name, bool warn) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find parameter " PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			 ", using double NaN", name);
    return(nan(""));
  }
  return(dv->rpt_double);
}

/**
 * @brief Get the double array parameter from the table.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @return The value of the parameter; NULL if not found
 */
double *rp_parameter_double_ptr(struct rp_keyvalp *table, char *name, bool warn) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find double float array parameter"
			 PARAM_NAME_BEGIN "%s" PARAM_NAME_END, name);
    return(NULL);
  }
  return(dv->rpt_double_ptr);
}

/**
 * @brief Get the double array parameter as a GSL block pointer from
 * the table.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @return The value of the parameter; NULL if not found
 */
gsl_block *rp_parameter_double_gsl(struct rp_keyvalp *table, char *name, bool warn) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find double float array parameter"
			 PARAM_NAME_BEGIN "%s" PARAM_NAME_END, name);
    return(NULL);
  }
  return(dv->rpt_double_gsl);
}

/**
 * @brief Get the single float array parameter from the table.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @return The value of the parameter; NULL if not found
 */
float *rp_parameter_float_ptr(struct rp_keyvalp *table, char *name, bool warn) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find parameter " PARAM_NAME_BEGIN "%s" PARAM_NAME_END, name);
    return(NULL);
  }
  return(dv->rpt_float_ptr);
}

/**
 * @brief Get the string parameter from the table; returns NULL if not found.
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @return The value of the parameter; NULL if not found
 */
char *rp_parameter_string(struct rp_keyvalp *table, char *name, bool warn) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find parameter " PARAM_NAME_BEGIN "%s" PARAM_NAME_END, name);
    return(NULL);
  }
  return(dv->rpt_string);
}

/**
 * @brief Get the bool parameter from the table
 *
 * @param[in] table The parameter table
 * @param[in] name  The name of the parameter
 * @param[in] warn  Warn if @c WARN_ON and the parameter could not be found; do not warn if @c WARN_OFF
 * @param[in] default_val The value to return if the parameter cannot be found in the table
 * @return The value of the parameter or default_val if it could not be found in the table
 */
bool rp_parameter_bool(struct rp_keyvalp *table, char *name, bool warn, bool default_val) {
  union datum_value *dv = find_dv(table, name);
  if (dv==NULL) {
    if (warn) dzlog_warn("Cannot find parameter "
			  PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			 " returning default value "
			 PARAM_VALUE_BEGIN "%s" PARAM_VALUE_END,
			 name,
			 default_val ? "true" : "false");
    return(default_val);
  }
  return(dv->rpt_bool);
}

/**
 * @brief Find the members (a hash table) of this parameter group
 *
 */
struct rp_keyvalp *rp_group_members(struct rp_keyvalp *group) {
  return ((struct rp_keyvalp *)group->value->rpt_members);
}

/************************************************************************
 * Hash tables
************************************************************************/

/**
 * @brief Create a table of parameter names and the value of one
 * property of that parameter, represented by an integer, such as the
 * type or number of elements.
 *
 * @param[in] parameters The array of parameters
 * @param[in] property   The property to set, identified by the byte offset in <tt>struct rp_parameter</tt>
 * @param[in] numitems   The total number of parameters to set
 */
struct rp_keyvalp *rp_make_parameter_properties_table
(struct rp_parameter *parameters, size_t property, size_t numitems) {
  // Declare the table
  struct rp_keyvalp *table=NULL, *row;
  // Add the parameters to the table
  for (size_t i=0; i<numitems; i++) {
    row = make_keyvalp(&table, parameters[i].name, NULL);
    if (row == NULL) return(NULL);
    int val = *(int *)((uint8_t*)&parameters[i]+property);
    row->value->rpt_int = val;
  }
  return(table);
}

/**
 * @brief Make a hash table with sequential integers, analogous to a C enum. To find the sequence number for a particular item, use `rp_parameter_int()`. To find the inverse (i.e., find the string for particular sequence number), use standard array indexing into the `names` array.
 *
 * @param[in] names The names of the items in order
 * @param[in] numitems The size of the names array
 * @param[in] start The integer to start the sequence; customarily 0.
 */
struct rp_keyvalp *rp_make_sequential_table(const char **names, int numitems, int start)
{
  // Declare the table
  struct rp_keyvalp *table=NULL, *row;
  // Add the parameters to the table
  for (int i=start; i<start+numitems; i++) {
    row = make_keyvalp(&table, names[i], NULL);
    if (row == NULL) return(NULL);
    row->value->rpt_int = i;
  }
  return(table);
}

/**
 * @brief Find the category of the first parameter in the table of
 * values, and optionally check that all the parameters in that
 * category are present.
 *
 * @param[in] parameter_values     Table of parameters and values
 * @param[in] parameter_properties Table of all parameters and their properties
 * @param[in] complete             Check that table has all properties in the category, and warn if any missing
 * @return                         The category (integer)
 */
int rp_find_category
(struct rp_keyvalp *parameter_values, struct rp_keyvalp *parameter_properties, bool complete)
{
  struct rp_keyvalp *step, *tmp;
  int categ = 0;
  HASH_ITER(hh, parameter_properties, step, tmp) {
    if (categ == 0) {
      if (find_dv(parameter_values, step->key) != NULL) categ = step->value->rpt_int;
      else if (complete
	       && step->value->rpt_int == (int)categ
	       && find_dv(parameter_values, step->key) == NULL)
	{
	  dzlog_error("Cannot find parameter %s in table", step->key);
	  return(-1);
	}
    }
  }
  return(categ);
}

#pragma GCC visibility pop

/**
 * @brief Make the key-value pair and add it to a table.
 *
 * @param[in] table     The table that this key-value pair (parameter name and value) will be added to
 * @param[in] key       The name of the parameter
 * @param[in] group     The group to which this parameter belongs
 *
 */
struct rp_keyvalp *make_keyvalp
(struct rp_keyvalp **table, const char *key, struct rp_keyvalp *group)
{
  struct rp_keyvalp *s = GC_MALLOC(sizeof(struct rp_keyvalp));
  if (s == NULL) return(NULL);
  strncpy(s->key, key, KEYLEN);
  HASH_ADD_STR(*table, key, s);
  if (group != NULL) {
    group->value->rpt_members = *table;
  }
  union datum_value * dv = GC_MALLOC(sizeof(union datum_value));
  if (dv == NULL) {
    dzlog_error("Cannot malloc dv");
    return(NULL);
  }
  s->value = dv;
  return(s);
}
