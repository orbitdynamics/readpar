###################
## Project name  ##
###################

project(readpar)

##############################
## CMake settings and paths ##
##############################

cmake_minimum_required(VERSION 3.13)

MESSAGE(STATUS "Use -DLOCAL_PREFIX, e.g., -DLOCAL_PREFIX=\"/home/user/.local\", to define installation location.")

if(DEFINED LOCAL_PREFIX)
  include_directories(${LOCAL_PREFIX}/include)
  link_directories(${LOCAL_PREFIX}/lib)
endif(DEFINED LOCAL_PREFIX)

# Path for Find*.cmake and other utils.
set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake-lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})

# Specify the output location of the static library. NOTE: ARCHIVE_OUTPUT_DIRECTORY is used for
# static libraries whereas LIBRARY_OUTPUT_DIRECTORY is only for shared libs.
#set_property(GLOBAL PROPERTY ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/../lib/)

## Build Options ##
# Enforce the C standards.
set(CMAKE_C_STANDARD_REQUIRED YES)
set(CMAKE_C_STANDARD 11)

# https://stackoverflow.com/a/24767451/11889224
set(CMAKE_BUILD_TYPE Debug)
# message("${BoldMagenta}CMAKE_C_FLAGS_DEBUG is ${CMAKE_C_FLAGS_DEBUG}${ColourReset}")

# https://stackoverflow.com/a/50882216/11889224
# -Wall:     Enables all compiler's warning messages.
# -Wextra:   Adds extra warnings, like if a comparison is always true or always false
#            due to the limited range of the data type
# -pedantic: Stricter enforment of warnings causes some of them to be turn to errors,
#	         particuarly warnings that go against ISO standards.
# -Werror:   Turns all warnings into errors and prevents compiling with warnings.
add_compile_options(-Wall -Wextra -pedantic )

##############################
## Components and libraries ##
##############################

## Glob all the .c and .h files in the directory.
file(GLOB READPARC "*.c")
file(GLOB READPARH "readpar.h" "hash.h" "yaml.h")

find_package(GSL REQUIRED)
find_package(LibGc REQUIRED) # Find LibGc (custom function), required.
find_file(UTHASH_HEADER "uthash.h" REQUIRED) # Check that uthash header file is present
find_library(YAML_LIBRARY REQUIRED NAMES yaml)
find_library(ZLOG_LIBRARY REQUIRED NAMES zlog)

add_library(readpar SHARED ${READPARC}) # Serial library.
# Set the visibility property to the equivalent of `-fvisibility=hidden`.
# This hides functions used only internally from the public interface.
set_target_properties(readpar PROPERTIES C_VISIBILITY_PRESET hidden)

# Install the library to /usr/local/lib/libreadpar.so with
#  sudo cmake --install .
# Or to another location with
#  mkdir -p ~/local/lib
#  cmake --install . --prefix ~/local/
install(TARGETS readpar)
install(FILES ${READPARH} DESTINATION include/readpar)

target_link_libraries(readpar ${GC_LIBRARY}
                                       ${YAML_LIBRARY}
                                       ${ZLOG_LIBRARY}
                                       ${GSL_LIBRARIES}
                                       ${LIBGC_LIBRARIES})

# Build documentation if the necessary processing programs are found
find_package(Doxygen
             REQUIRED dot
             OPTIONAL_COMPONENTS mscgen dia)
if(DOXYGEN_FOUND)
  add_subdirectory ("doxygen")
  find_package(Sphinx)
  if(SPHINX_FOUND)
    add_subdirectory ("sphinx")
  endif(SPHINX_FOUND)
endif(DOXYGEN_FOUND)
