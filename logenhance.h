/* Liam Healy 2017-08-18 14:15:54CDT colorlog.h */
/* Time-stamp: <2021-08-20 12:33:59EDT logenhance.h> */
// https://stackoverflow.com/a/3219471
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define FILE_NAME_BEGIN ANSI_COLOR_MAGENTA
#define FILE_NAME_END ANSI_COLOR_RESET
#define PARAM_NAME_BEGIN ANSI_COLOR_GREEN
#define PARAM_NAME_END ANSI_COLOR_RESET
#define PARAM_VALUE_BEGIN ANSI_COLOR_RED
#define PARAM_VALUE_END ANSI_COLOR_RESET
#define PARAM_TYPE_BEGIN ANSI_COLOR_BLUE
#define PARAM_TYPE_END ANSI_COLOR_RESET
