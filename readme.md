Read parameters (readpar) is a C library that will read small pieces of data from a text file, and place them in a hash table for use by a C program.

Documentation is [available](Documentation.html) once built; to build, follow these steps

1. Install the [zlog](https://github.com/HardySimpson/zlog "zlog github home page") logger

    ```bash
    git clone https://github.com/HardySimpson/zlog.git
    cd zlog
    make PREFIX=/usr/local/
    sudo make PREFIX=/usr/local/ install
    ```
1. Install Debian packages

    sudo apt install cmake gcc make
    sudo apt install libyaml-dev uthash-dev
    sudo apt install libgc-dev
    sudo apt install libgsl-dev
    sudo apt install python3-breathe doxygen graphviz

1. Build

    rm -rf build/*
    cd build
    cmake ..
    make
    sudo cmake --install .
