/* Parse a YAML file  */
/* Liam Healy 2018-06-03 09:47:47EDT yaml.c */
/* Time-stamp: <2021-09-23 19:11:01EDT yaml.c> */

// gcc -g yaml.c -lyaml -o yaml

#include <gc/gc.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <yaml.h>
#include <fnmatch.h>
#include "zlog.h"
#include "yaml.h"
#include "logenhance.h"
#define NOGROUP "not in group"
#define MAX_PARSE_ERRORS 10

/**
 * @brief Interpret various strings as being boolean "true" or "false".
 */
static bool read_bool(char *string)
{
  if (strcmp(string,"false")==0 || strcmp(string,"no")==0 ||
      strcmp(string,"FALSE")==0 || strcmp(string,"NO")==0) return(false);
  else return(true);
}

/**
 * @brief Read the character @c string and save the data.
 *
 * @param[out] dv The value of the parameter
 * @param[in] string The character string to be read
 * @param[in] type The type of data
 * @param[in] index The index in an array, if applicable
 * @param[in] size The size of the array, if applicable
 */
static void parse_data(union datum_value *dv, char *string, enum rp_type_list type, size_t index, size_t size) {
  switch(type)
    {
      case rp_type_int:
	dv->rpt_int = atoi(string);
	dzlog_debug("Integer value read " PARAM_VALUE_BEGIN "%d" PARAM_VALUE_END, dv->rpt_int);
	break;
      case rp_type_double:
	dv->rpt_double = atof(string);
	dzlog_debug("Double value read " PARAM_VALUE_BEGIN "%f" PARAM_VALUE_END, dv->rpt_double);
	break;
      case rp_type_double_ptr:
	if (strcmp(string,"none")==0 || strcmp(string,"NONE")==0) {
	  dv->rpt_double_ptr = NULL;
	} else {
	  if (index == 0) {
	    double *vector = GC_MALLOC(size*sizeof(double));
	    dv->rpt_double_ptr = vector;
	  }
	  if (index < size) {
	    dv->rpt_double_ptr[index] = atof(string);
	  } else {
	    dv->rpt_double_ptr = GC_REALLOC(dv->rpt_double_ptr,(index+1)*sizeof(double));
	    dv->rpt_double_ptr[index] = atof(string);
	  }
	  dzlog_debug("Double value read " PARAM_VALUE_BEGIN "%f" PARAM_VALUE_END " at index %zu", dv->rpt_double_ptr[index], index);
	}
	break;
      case rp_type_double_vec:
	if (strcmp(string,"none")==0 || strcmp(string,"NONE")==0) {
	  dv->rpt_double_gsl = NULL;
	} else {
	  if (index == 0) {
	    gsl_block *gblk = GC_MALLOC(sizeof(gsl_block));
	    gblk->data = GC_MALLOC(sizeof(double));
	    gblk->size = 1;
	    dv->rpt_double_gsl = gblk;
	  }
	  if (index >= dv->rpt_double_gsl->size) {
	    dv->rpt_double_gsl->size = index+1;
	    dv->rpt_double_gsl->data = GC_REALLOC(dv->rpt_double_gsl->data, dv->rpt_double_gsl->size*sizeof(double));
	  }
	  dv->rpt_double_gsl->data[index] = atof(string);
	  dzlog_debug("Double vector value read " PARAM_VALUE_BEGIN "%f" PARAM_VALUE_END " at index %zu", dv->rpt_double_gsl->data[index], index);
	}
	break;
      case rp_type_float_ptr:
	if (strcmp(string,"none")==0 || strcmp(string,"NONE")==0) {
	  dv->rpt_float_ptr = NULL;
	} else {
	  if (index == 0) {
	    float *vector = GC_MALLOC(size*sizeof(float));
	    dv->rpt_float_ptr = vector;
	  }
	  if (index < size) {
	    dv->rpt_float_ptr[index] = atof(string);
	  } else {
	    dv->rpt_float_ptr = GC_REALLOC(dv->rpt_float_ptr,(index+1)*sizeof(float));
	    dv->rpt_float_ptr[index] = atof(string);
	  }
	}
	break;
      case rp_type_bool:
	dv->rpt_bool = read_bool(string);
	if (dv->rpt_bool) {
	  dzlog_debug("Boolean value read " PARAM_VALUE_BEGIN "true" PARAM_VALUE_END);
	} else {
	  dzlog_debug("Boolean value read " PARAM_VALUE_BEGIN "false" PARAM_VALUE_END);
	}
	break;
      case rp_type_char_ptr:
	dv->rpt_string = string;
	dzlog_debug("String value read " PARAM_VALUE_BEGIN "%s" PARAM_VALUE_END, dv->rpt_string);
	break;
      default:
	dzlog_error("Data type %d not found for " PARAM_VALUE_BEGIN "%s" PARAM_VALUE_END,
		    type, string);
    }
}

#pragma GCC visibility push(default)

/**
 * @brief Make a key-value table of parameter names and values with parameters read from a YAML file.
 *
 * @param[in] filename The name of the file to read from
 * @param[in] type_table The table of element types for each of the parameters
 * @param[in] length_table The table of sizes (number of elements) of parameters
 * @return The key-value table
 */
struct rp_keyvalp *rp_read_parameter_yaml
(char *filename, struct rp_keyvalp *type_table, struct rp_keyvalp *length_table) {
  // See https://www.wpsoftware.net/andrew/pages/libyaml.html
  FILE *fh = fopen(filename, "r");
  if(fh == NULL) {
    dzlog_error("Failed to open file %s, name has %d characters", filename, (int)strlen(filename));
    return(NULL);
  }

  yaml_parser_t parser;
  yaml_token_t  token;

  /* Initialize parser */
  if(!yaml_parser_initialize(&parser)) {
    dzlog_error("Failed to initialize parser");
    return(NULL);
  }

  /* Set input file */
  yaml_parser_set_input_file(&parser, fh);

  struct rp_keyvalp *value_table=NULL, *groupptr=NULL, *param_table=NULL;
  union datum_value *dv;
  char lastkey[KEYLEN], groupname[KEYLEN];
  int datatype;
  int length;
  bool keytoken;
  size_t fsindex;
  strcpy(lastkey,"");
  strcpy(groupname, NOGROUP);
  int num_parse_errors = MAX_PARSE_ERRORS;
  do {
    if (num_parse_errors <=0) {
      return(NULL);
    }
    yaml_parser_scan(&parser, &token);
    switch(token.type)
      {
	/* Stream start/end */
      case YAML_STREAM_START_TOKEN: break;
      case YAML_STREAM_END_TOKEN: break;
	/* Token types (read before actual token) */
      case YAML_KEY_TOKEN:
	keytoken = true;
	break;
      case YAML_VALUE_TOKEN:
	keytoken = false;
	break;
	/* Block delimiters */
      case YAML_BLOCK_SEQUENCE_START_TOKEN:
	break;
      case YAML_BLOCK_ENTRY_TOKEN:
	break;
      case YAML_BLOCK_END_TOKEN:
	strcpy(groupname, NOGROUP);
	break;
      case YAML_FLOW_SEQUENCE_START_TOKEN:
	break;
      case YAML_FLOW_SEQUENCE_END_TOKEN:
	/* for (size_t i=0; i<fsindex; i++) {
	   printf("stored key=%s[%zu], value=%f\n",s->key,i,s->value->fptr[i]);
	   } */
	break;
      case YAML_PARSE_FLOW_SEQUENCE_ENTRY_STATE:
	break;
	/* Data */
      case YAML_BLOCK_MAPPING_START_TOKEN:
	if (strcmp(lastkey,"") != 0) {
	  strcpy(groupname, lastkey);
	  groupptr = make_keyvalp(&value_table, groupname, NULL);
	  param_table=NULL;
	  dzlog_debug("Reading into"
		      PARAM_TYPE_BEGIN " parameter group " PARAM_TYPE_END
		      PARAM_NAME_BEGIN "%s" PARAM_NAME_END
		      " ("
		      PARAM_VALUE_BEGIN "%p" PARAM_VALUE_END
		      ")",
		      groupname, (void *)groupptr);
	}
	break;
      case YAML_SCALAR_TOKEN:
	if (keytoken) {	/* Key token */
	  strncpy(lastkey,(char *)token.data.scalar.value,KEYLEN);
	  fsindex=0;
	} else {		/* Value token */
	  char *v = GC_MALLOC(VALLEN * sizeof(char));
	  strncpy(v,(char *)token.data.scalar.value,VALLEN);
	  if (fsindex == 0) {
	    datatype = rp_parameter_int(type_table, lastkey, WARN_OFF, -1);
	    if (datatype < 0) {
	      // Check for obsolete parameters
	      // It would probably be better to make an "obsolete parameters" table and check against that.
	      if (strcmp(lastkey,"solution-bvp") == 0) {
		dzlog_error("Keyword " PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			    " is obsolete; parameter file needs to be updated", lastkey);
	      } else {
		dzlog_error("Unrecognized keyword " PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			    " with value " PARAM_VALUE_BEGIN "%s" PARAM_VALUE_END, lastkey, v);
	      }
	      // The keyword was not found in the table, so exit
	      break;
	    }
	    length = rp_parameter_int(length_table, lastkey, WARN_OFF, -1);
	    struct rp_keyvalp *kvp = make_keyvalp(&param_table,lastkey,groupptr);
	    if (kvp != NULL) dv = kvp->value;
	    dzlog_debug("Reading"
			PARAM_TYPE_BEGIN " PARAM key/value " PARAM_TYPE_END
			"for key "
			PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			" in group "
			PARAM_NAME_BEGIN "%s" PARAM_NAME_END
			" ("
			PARAM_VALUE_BEGIN "%p" PARAM_VALUE_END
			")",
			lastkey, groupname, (void *)groupptr);
	  }
	  parse_data(dv, v, datatype, fsindex, length);
	  fsindex++;
	}
	break;
	/* Others */
      default:
	dzlog_warn("Don't know what to do with"
		   PARAM_TYPE_BEGIN " PARAM token " PARAM_TYPE_END
		   "of type "
		   PARAM_VALUE_BEGIN "%d" PARAM_VALUE_END,
		   token.type);
	--num_parse_errors;
      }
    if(token.type != YAML_STREAM_END_TOKEN)
      yaml_token_delete(&token);
  } while(token.type != YAML_STREAM_END_TOKEN);
  yaml_token_delete(&token);
  /* Cleanup */
  yaml_parser_delete(&parser);
  fclose(fh);
  dzlog_debug("Found "
	      PARAM_VALUE_BEGIN "%d" PARAM_VALUE_END
	      PARAM_TYPE_BEGIN " PARAM groups " PARAM_TYPE_END
	      "in file "
	      PARAM_NAME_BEGIN "%s" PARAM_NAME_END,
	      HASH_COUNT(value_table),filename);
  return(value_table);
}

#pragma GCC visibility pop
