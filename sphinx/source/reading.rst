******************
Reading data files
******************

Parameter definition
====================

Each parameter to be read needs to be defined in advance, as a name, datum type, size in number of elements, and optionally category. This may be done with a literal array, for example,

.. code-block:: c

   struct rp_parameter confparm[]
      = {{"name", rp_type_char_ptr, 1, NO_CATEGORY},
         {"position",  rp_type_double_ptr, 3, NO_CATEGORY},
	 {"velocity",  rp_type_double_ptr, 3, NO_CATEGORY},
	 {"epoch", rp_type_char_ptr, 1, NO_CATEGORY}}

to define four parameters, with two strings and two 3-vectors. The object made is a structure.

.. doxygenstruct:: rp_parameter
   :members:

The datum types are one of the following

.. doxygenenum:: rp_type_list

Each of the three tables for ``type``, ``size``, and ``category`` can be defined by calls to :code:`rp_make_parameter_properties_table()` (see :ref:`reading:Special integer tables`).

.. code-block:: c

  numargs=sizeof(confparm)/sizeof(struct rp_parameter);
  struct rp_keyvalp *types_table	/**< Table of parameter names and their data types  */
    = rp_make_parameter_properties_table(confparm, offsetof(struct rp_parameter, type), numargs);
  struct rp_keyvalp *lengths_table	/**< Table of parameter names and the number of scalar elements */
    = rp_make_parameter_properties_table(confparm, offsetof(struct rp_parameter, size), numargs);
  struct rp_keyvalp *category_table /**< Table of parameter names and their parameter categories */
    = rp_make_parameter_properties_table(confparm, offsetof(struct rp_parameter, category), numargs);

Reading parameters
==================

A YAML input file with a set of parameters may be read with :code:`rp_read_parameter_yaml()` to produce a :ref:`table <using:Table>`.

.. doxygenfunction:: rp_read_parameter_yaml
   :project: readpar

Once read, the values may be used in a program with the functions :doc:`described <using>`.

Groups and categories
=====================

Parameters may be grouped together in the YAML file with an indented block. The name give to this block is the string at the header of the block. The members of this group are a table which is found with :code:`rp_group_members()`. If categories are defined for the parameters, then, assuming the members of the group belong to the same category, this category may be identified with :code:`rp_find_category()`

To iterate over the groups, use the macro :code:`HASH_ITER()`

.. code-block:: c

   struct rp_keyvalp *table = rp_read_parameter_yaml(yaml_filename,types_table,lengths_table);
   int num_groups = HASH_COUNT(table); /* The number of top-level items read */
   struct rp_keyvalp *table,	/**< The parameter names and values to be read from the file */
   *group,			/**< The YAML group being processed in hash iteration */
   *tmp;
   HASH_ITER(hh, table, group, tmp) {
    switch(rp_find_category(rp_group_members(group), category_table, false))
    {
		/* Case blocks for each category */
    }
   }


.. doxygenfunction:: rp_group_members
   :project: readpar

.. doxygenfunction:: rp_find_category
   :project: readpar

Special integer tables
======================

To make tables of properties represented by integers, use :code:`rp_make_parameter_properties_table()`. To make a table of sequential integers, analogous to a C :code:`enum`, use :code:`rp_make_sequential_table()`.

.. doxygenfunction:: rp_make_parameter_properties_table
   :project: readpar

.. doxygenfunction:: rp_make_sequential_table
   :project: readpar
