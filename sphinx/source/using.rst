**********
Using data
**********

Functions
=========

Each of these functions will return the datum of the specified type from a pointer to the table and the name of the parameter. Optionally, a warning will be given if the parameter named is not found in the table.

.. doxygenfunction:: rp_parameter_bool
   :project: readpar

.. doxygenfunction:: rp_parameter_double
   :project: readpar

.. doxygenfunction:: rp_parameter_double_gsl
   :project: readpar

.. doxygenfunction:: rp_parameter_double_ptr
   :project: readpar

.. doxygenfunction:: rp_parameter_float_ptr
   :project: readpar

.. doxygenfunction:: rp_parameter_int
   :project: readpar

.. doxygenfunction:: rp_parameter_string
   :project: readpar

Table
=====

The key-value table with the parameters is represented by a :code:`rp_keyvalp` structure, which is created by :code:`rp_read_parameter_yaml` (see :ref:`reading:Reading parameters`). If parameters are in :ref:`groups <reading:Groups and categories>`, the name of the group is a key in the table with an value associated that contains the members of the group, obtained with :code:`rp_group_members()`.

.. doxygenstruct:: rp_keyvalp
