*****************
Build and install
*****************

Readpar requires a `C11 compiler <https://en.wikipedia.org/wiki/C11_(C_standard_revision)>`_, `CMake <https://cmake.org/>`_, and uses the following libraries or include files
  * `libyaml <https://github.com/yaml/libyaml>`_
  * `uthash <https://troydhanson.github.io/uthash/userguide.html>`_ (include file only)
  * `Boehm GC <https://www.hboehm.info/gc/>`_
  * `GSL <https://www.gnu.org/software/gsl/>`_
  * `zlog <https://github.com/HardySimpson/zlog>`_
  * Optional: `Sphinx <https://www.sphinx-doc.org/en/master/index.html>`_, `Doxygen <https://www.doxygen.nl/index.html>`_, and `Breathe <https://breathe.readthedocs.io>`_ to build this documentation

With root privilege, install to the conventional location ``/usr/local`` this is the default without any ``--prefix`` argument (see below). Without root privilege, define the installation location in a different place, for example::

  export USRLOCAL=/home/me/usrlocal

Install zlog (the last command requires ``sudo`` if installing to ``/usr/local``)::

  git clone https://github.com/HardySimpson/zlog.git
  cd zlog
  make PREFIX=$USRLOCAL
  make PREFIX=$USRLOCAL install

For the Debian family of operating systems, the following commands will install everything else needed or optional::

  sudo apt install cmake gcc make
  sudo apt install libyaml-dev uthash-dev
  sudo apt install libgc-dev
  sudo apt install libgsl-dev
  sudo apt install python3-breathe doxygen graphviz

.. Red Hat family
.. sudo yum install gc-devel
.. sudo yum install libyaml-devel uthash-devel


Use CMake to build and install::

  cd build
  rm -rf ../build/*
  cmake .. -DLOCAL_PREFIX=$USRLOCAL
  make
  cmake --install . --prefix $USRLOCAL

where the |--prefix|_ option will install the ``readpar`` library and include file in that location; if installing to ``/usr/local``, it is not necessary to specify this option, but a ``sudo`` will be necessary::

  sudo cmake --install .

.. See nested formatting in ReST https://stackoverflow.com/q/4743845/238405
.. |--prefix| replace:: ``--prefix`` option
.. _--prefix: https://cmake.org/cmake/help/latest/guide/tutorial/Installing%20and%20Testing.html

This documentation is available by opening the file ``Documentation.html`` in the source directory in a web browser.
