.. readpar documentation master file, created by
   sphinx-quickstart on Thu Sep 16 17:15:33 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree** directive.

*******
readpar
*******

Introduction
===================================

Read parameters (readpar) is a C library that will read small pieces of data from a text file, and place them in a hash table for use by a C program. The format of the file is plain-text `YAML <https://en.wikipedia.org/wiki/YAML>`_. There are several functions provided that will read the parameters as C variables. Parameters are read as groups, represented by YAML blocks. Parameters groups can be designated to belong to categories to aid in processing. These categories will automatically be detected.

:doc:`build` describes dependencies and has build and install instructions.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   build
   reading
   using

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
